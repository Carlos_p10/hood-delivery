import 'package:hoodDelivery/src/pages/Home.dart';
import 'package:hoodDelivery/src/pages/evaluatePage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderDescriptionPage extends StatefulWidget {
  @override
  _OrderDescriptionPageState createState() => _OrderDescriptionPageState();
}

class _OrderDescriptionPageState extends State<OrderDescriptionPage> {
  Color myColor = Color(0xff00bfa5);
  final _style = GoogleFonts.muli(
      color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFFAFAFA),
          elevation: 3,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(FontAwesomeIcons.chevronLeft),
            color: Colors.black87,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'Descripcion',
            style: GoogleFonts.muli(
              color: Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        body: ListView(
          //mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10.0),
            _posterTitulo(),
            SizedBox(height: 10.0),
            _descripcion('Restaurante: ',
                'Colonia Escalon, San Salvador, Tacos Tapatios '),
            // SizedBox(height: 1.0),
            _descripcion('Telefono: ', '2525-2525'),
            SizedBox(height: 10.0),
            _descripcion('Orden: ', '5 Tacos al Pastor con Chorizo Extra'),
            SizedBox(height: 10.0),
            _descripcion(
                'Cliente: ', 'Colonia Masferrer, San Salvador, por Roots'),
            // SizedBox(height: 5.0),
            _descripcion('Telefono: ', '6169-7080'),
            // _descripcion(),
            SizedBox(height: 5.0),
            _botonEvaluar(),
            SizedBox(height: 5.0),
            _botonFinalizar(),
          ],
        ));
  }

  Widget _posterTitulo() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(
                    'https://d1kxxrc2vqy8oa.cloudfront.net/wp-content/uploads/2020/01/09214916/RFB-2312-2-tacos.jpg'),
                height: 120.0,
              ),
            ),
          ),
          SizedBox(width: 20.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Tacos al Pastor',
                    style: GoogleFonts.muli(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w500)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _descripcion(String title, String descripcion) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(title,
                textAlign: TextAlign.start,
                style: GoogleFonts.muli(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w500)),
            SizedBox(
              height: 5,
            ),
            Text(descripcion,
                textAlign: TextAlign.start,
                style: GoogleFonts.muli(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w300)),
          ],
        ));
  }

  Widget _botonFinalizar() {
    return Container(
      alignment: Alignment.center,
      child: FlatButton(
          onPressed: () {
            setState(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                });
          },
          textColor: Colors.blue,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.black)),
          color: Colors.white,
          child: Text(
            'Finalizar',
            style: _style,
          )),
    );
  }

  Widget _botonEvaluar() {
    return Container(
      alignment: Alignment.center,
      child: FlatButton(
          onPressed: () {
            setState(() {
              openAlertBox(context, myColor);
            });
          },
          textColor: Colors.blue,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.black)),
          color: Colors.white,
          child: Text(
            'Evaluar',
            style: _style,
          )),
    );
  }
}
