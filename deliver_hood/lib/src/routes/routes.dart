
import 'package:hoodDelivery/src/pages/login_page.dart';
import 'package:hoodDelivery/src/pages/registro_page.dart';
import 'package:flutter/material.dart';
import 'package:hoodDelivery/src/pages/HomePage.dart';


Map<String, WidgetBuilder> getApplicationRoutes(){
  return<String, WidgetBuilder>{
    '/' : (BuildContext context) => HomePage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegistroPage(),

  };
}