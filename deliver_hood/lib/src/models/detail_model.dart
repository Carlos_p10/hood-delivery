import 'dart:convert';

import 'package:hoodDelivery/src/models/menu_models.dart';
import 'package:hoodDelivery/src/models/user_model.dart';


Details detailsFromJson(String str) => Details.fromJson(json.decode(str));

String detailsToJson(Details data) => json.encode(data.toJson());

class Details {
    Details({
        this.ok,
        this.msg,
        this.data,
    });

    bool ok;
    String msg;
    List<Detail> data;

    factory Details.fromJson(Map<String, dynamic> json) => Details(
        ok: json["ok"],
        msg: json["msg"],
        data: List<Detail>.from(json["data"].map((x) => Detail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Detail {
    Detail({
        this.cantidad,
        this.estado,
        this.status,
        this.id,
        this.usuario,
        this.empresa,
        this.menu,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    int cantidad;
    String estado;
    bool status;
    String id;
    User usuario;
    User empresa;
    Menu menu;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        cantidad: json["cantidad"],
        estado: json["estado"],
        status: json["status"],
        id: json["_id"],
        usuario: json["usuario"] == null ? null : User.fromJson(json["usuario"]),
        empresa: json["empresa"] == null ? null : User.fromJson(json["empresa"]),
        menu: json["menu"] == null ? null : Menu.fromJson(json["menu"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "cantidad": cantidad,
        "estado": estado,
        "status": status,
        "_id": id,
        "usuario": usuario == null ? null : usuario.toJson(),
        "empresa": empresa == null ? null : empresa.toJson(),
        "menu": menu == null ? null : menu.toJson(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}