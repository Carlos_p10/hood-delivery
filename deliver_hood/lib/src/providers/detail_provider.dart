import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:hoodDelivery/src/models/detail_model.dart';

class DetailProvider {
  Response resp;
  final dio = new Dio();
  final String _url = 'https://dev-hood.herokuapp.com/ws/details';

  final opts = Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      });

  Future<List<Detail>> _procesarRespuesta(String url) async {
    final resp = await dio.get(url, options: opts);
    final decodedData = resp.data;
    final details = new Details.fromJson(decodedData);
    // print(details.msg);
    // print(details.data[0].cantidad);
    return details.data;
  }

  Future<List<Detail>> getDetail() async {
    final String url = '$_url/all';
    return await _procesarRespuesta(url);
  }

  Future<Map<String, dynamic>> postNewDetail(
      String userCode, String bussinesCode, String menuId, int cantidad) async {
    final String url = '$_url/new';
    final newDetail = {
      'usuario': userCode,
      'empresa': bussinesCode,
      'menu': menuId,
      'cantidad': cantidad,
    };
    resp = await dio.post(url, data: json.encode(newDetail), options: opts);

    Map<String, dynamic> decodedResp = resp.data;
    return {'ok': true, 'token': decodedResp['msg']};
  }
}
